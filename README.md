# Video Store Kata in Swift

Video Store Kata in Swift

See the 'master' branch for the starting point of the kata.

My first solution is in the 'solution1' branch.

This repo is based on Clean Coders Episode 3 "Functions" and 3.3 "Video Store".

The original repo is in Java, so I converted to Swift in order to follow along with
the video store episode in a language I'm familiar with.

Clean Goders Epsiodes (Paid):

https://cleancoders.com/episode/clean-code-episode-3

https://cleancoders.com/episode/clean-code-episode-3-sc-3-videostore

Github Repo:

https://github.com/unclebob/videostore
